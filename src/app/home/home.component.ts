import { Component, OnInit, TemplateRef, Input } from '@angular/core';
import { StudentService } from '../services/student.service';
import { User } from '../models/user';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  @Input() student: any[];
  classes$: any[];
  modalRef: BsModalRef;
  isLoginError: boolean;
  isStudent: boolean;
  isLoginSuccessful: boolean;
  isOpen: boolean;
  status: any;
  subject$: any;
  activeClassPass: string = '';
  class: any[];
  course: any[];
  constructor(private studentService: StudentService, private router: Router,
    private modalService: BsModalService,
    private dataSrv: DataService) { }
  
  
    openModal(template: TemplateRef<any>, classPass: string, data: any) {
    let existing = this.dataSrv.getData()
    existing.classID = data.classID;
    existing.time = data.date;
    this.dataSrv.setData(existing);
    this.activeClassPass = classPass;
    this.modalRef = this.modalService.show(template);
  }
  
  ngOnInit() {

    return this.getClass();
  }

  getClass(): void {
    this.studentService.getClassesToday()
      .subscribe(s => {

        return this.classes$ = s


      });
  }
  onSingUp(form: NgForm) {

    const classPass = form.value.classPass;
    if (form.value.classPass == this.activeClassPass) {
      // this.studentService.classLogin(classPass).subscribe((data : any)=>{
      //   console.log(data);
        // if(data) {
          let data  = this.dataSrv.getData()
          this.studentService.addStudentAtte(data).subscribe(
            (res) => {
              alert("Uspešno ste potvrdili vaše prisustvo!")
            }
          )
      // } 
      //  else 
      //  {
      //    alert("pogresno korsnicko ime i/ili lozinka")
      //  }
       
     }
    //  (err : HttpErrorResponse)=>{
    //    this.isLoginError = true;
    //  });
    else {
      alert("Pogrešna lozinka!!!")

    }



  }



  getSubjectNameID(): void {
    this.studentService.getSubjectName().subscribe(data => {
      this.subject$ = data
    })
  }

 /*  add(data: any): void {
    let request = {
      time: data.value.time,
      studentID: data.value.studentID,
      classID: data.value.classID
    }
    this.studentService.addStudentAtte(request)
      .subscribe(data => {
        if (data == true) {
          this.getAttendances();
          this.class.push(data);
        } else {
          alert("Morate popuniti sva polja!!!")
        }
      });
    } */

    getAttendances() {
      this.studentService.getAttendance().subscribe(data => {
        this.course = data
      });
    }

}
