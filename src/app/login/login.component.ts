import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../services/student.service';
import { Login } from '../models/userLogin';
import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  isLoginError: boolean;
  isStudent: boolean;
  student: any;
  isLoginSuccessful: boolean;
  constructor(
    private router: Router,
    private serviceStudent: StudentService,
    public dataSrv: DataService) { }
    status : any;
   
  ngOnInit() {  }

onSingUp(form: NgForm){
    const email = form.value.email;
    const pass = form.value.pass;
    this.serviceStudent.getStudent().subscribe(d => {
      this.student = d;
      console.log(d);
    });
    this.serviceStudent.login(email,pass).subscribe((data : any)=>{
     let existing = this.dataSrv.getData();
     existing.studentID = data.studentID;
     this.dataSrv.setData(existing)
     if(data.isStudent == true && data.isLoginSuccessful == true) {
      this.router.navigate(['/home']);
     } 
     else if(data.isStudent == false && data.isLoginSuccessful == true) {
      this.router.navigate(['/subject']);
     }
     else if(data.isLoginSuccessful == false )
     {
        alert("pogresno korsnicko ime i/ili lozinka")
       
     }
     
   },
   (err : HttpErrorResponse)=>{
     this.isLoginError = true;
   });
 } 


  }

   





  

