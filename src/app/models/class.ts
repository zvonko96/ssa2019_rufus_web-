export class Class{
    classID: number;
    subjectID:number;
    date:Date;
    isLecture: boolean;
    lecturerID: number;
    isOpen: boolean; 
    classPass: string; 
    practiceGroup: string;
}