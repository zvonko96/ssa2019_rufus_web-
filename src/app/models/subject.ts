export class Subject{
   subjectName: string;
   professorID: number;
   assistantID: number;
   lectureNo: number;
   exerciseNo: number; 
   subjectDescription: string
}