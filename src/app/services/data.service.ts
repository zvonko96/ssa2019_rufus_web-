import { Injectable } from '@angular/core';
import { IData } from '../models/data';
import { Subject } from '../models/subject';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  data = new IData();
  constructor() { }


  getData() {
    return this.data;
  }

  setData (data) {
    this.data = data;
  }
}
