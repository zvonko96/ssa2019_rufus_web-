import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../models/user';
import { Login } from '../models/userLogin';
import { map, catchError } from 'rxjs/operators';
import { Subject } from '../models/subject'

import { environment } from '../../environments/environment';
import { Class } from '../models/class';

const headers = new HttpHeaders({
  'Content-Type': 'application/problem+json; charset=utf-8'
});

@Injectable({
  providedIn: 'root'
})

export class StudentService {
  errorData: {};
  isLoggedIn = false;
  Url = 'https://localhost:44374/api';

  constructor(private http: HttpClient) { }

  getSubjects(): Observable<Subject[]> {
    return this.http.get<Subject[]>(this.Url + '/subject');
  }



  /*    getSubject(professorID: number): Observable<any> {
     return this.http.get<any>(`${this.Url+'/subject'}/${professorID}`);
   }  */

  getSubject(professorID: number): Observable<Subject[]> {
    return this.http.get<Subject[]>('https://localhost:44374/api/subject/' + professorID);
  }



  getClasses(): Observable<Class[]> {
    return this.http.get<Class[]>(this.Url + '/class');
  }
  
  getClassesToday(): Observable<any> {
    return this.http.get<any>(this.Url + '/class/today');
  }

  getStudents(): Observable<any> {
    return this.http.get<any>(this.Url + '/student');
  }

  getAttendance(): Observable<any> {
    return this.http.get<any>(this.Url + '/Attendance');
  }



  login(email, password): Observable<any> {
    let request = {
      email: email,
      pass: password
    }
    //var data = "email=" + email + "&password=" + password ;
    JSON.stringify(request);
    return this.http.post(this.Url + '/login', request, { headers: headers });
  }

  addClass(hero: any): Observable<any> {
    return this.http.post<any>(this.Url + '/class/insert', hero);
  }

  updateClass(hero: any): Observable<any> {
    return this.http.put<any>(this.Url + '/class/update', hero);
  }

  addStudent(hero: any): Observable<any> {
    return this.http.post<any>(this.Url + '/student/insert', hero);
  }
  updateStudent(hero: any): Observable<any> {
    return this.http.put<any>(this.Url + '/student/update', hero);
  }

  deleteStudent(hero: any | number): Observable<any> {
    const studentID = typeof hero === 'number' ? hero : hero.studentID;
    const url = `${this.Url + "/student/delete"}/${studentID}`;

    return this.http.delete<any>(url)
  }

  deleteStudentAtend(hero: any | number): Observable<any> {
    const attendancesID = typeof hero === 'number' ? hero : hero.attendancesID;
    const url = `${this.Url + "/attendance/delete"}/${attendancesID}`;

    return this.http.delete<any>(url)
  }
  getSubjectName(): Observable<any> {
    return this.http.get<any>(this.Url+'/subject')
  }

  deleteHero(hero: any | number): Observable<any> {
    const classID = typeof hero === 'number' ? hero : hero.classID;
    const url = `${this.Url + "/class/delete"}/${classID}`;

    return this.http.delete<any>(url)
  }
  // updateSubject(subject: any): Observable<any> {
  //   return this.http.put(this.Url + '', subject);
  // }

  classLogin(classPass): Observable<any> {
    let request = {
      classPass: classPass
    }
    JSON.stringify(request);
    return this.http.post(this.Url + '/Class', request, { headers: headers });
  }

  addStudentAtte(hero: any): Observable<any> {
    return this.http.post<any>(this.Url + '/attendance/insert', hero);
  }

  getStudent():Observable<any>{
    return this.http.get<any>(this.Url + '/student');
  }

}
