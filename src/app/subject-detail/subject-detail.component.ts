import { Component, OnInit, Input } from '@angular/core';
import { StudentService } from '../services/student.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Subject } from '../models/subject'
import { Class } from '../models/class';

import { TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.css']
})
export class SubjectDetailComponent implements OnInit {
  modalRef: BsModalRef;
  modalReff: BsModalRef;
  subject: Subject[];
  class: Class[];
  student: any[];
  course: any[];

  openCloseButton: string = ''

  isLecture: boolean = false;
  isOpen: boolean = false;


  activeModal: string = ''
  constructor(
    private router: ActivatedRoute,
    private serviceStudent: StudentService,
    private location: Location,
    private modalService: BsModalService
  ) { }



  ngOnInit() {
    const userId = this.router.snapshot.paramMap.get('subjectID');
    this.GetTodosByUserId(parseInt(userId));
    this.getClass();
    this.getStudent();
    this.getAttendances();
  }

  GetTodosByUserId(lecturerID: number) {
    this.serviceStudent.getSubject(lecturerID).subscribe(d => {
      this.subject = d;
    });
  }

  goBack(): void {
    this.location.back();
  }

  getClass(): void {
    this.serviceStudent.getClasses().subscribe(data => {
      this.class = data
    });
  }

  getStudent() {
    this.serviceStudent.getStudents().subscribe(data => {
      this.student = data
    });
  }

  getAttendances() {
    this.serviceStudent.getAttendance().subscribe(data => {
      this.course = data
    });
  }

  openModal(template: TemplateRef<any>, type: string) {
    if (type == 'addModal') {
      this.activeModal = 'addModal';
    }
    else if (type == 'editModal') {
      this.activeModal = 'editModal';
    }
    else if (type == 'addStudent') {
      this.activeModal = 'addStudent';
    }
    else if (type == 'addStudentAtte') {
      this.activeModal = 'addStudentAtte';
    }
    else {
      this.activeModal = 'editStudent';
    }
    this.modalRef = this.modalService.show(template);
  }


  add(data: any): void {
    if (this.activeModal == 'addModal') {
      let request = {
        subjectID: data.value.subjectID,
        date: data.value.date,
        isLecture: this.isLecture,
        lecturerID: data.value.lecturerID,
        isOpen: this.isOpen,
        classPass: data.value.classPass,
        practiceGroup: data.value.practiceGroup
      }
      this.serviceStudent.addClass(request)
        .subscribe(data => {
          if (data == true) {
            this.isLecture = false;
            this.isOpen = false;
            this.getClass();
            this.class.push(data);
          } else {
            alert("Morate popuniti sva polja!!!")
          }
        });
    } else if (this.activeModal == 'editModal'){
      let request = {
        classID: data.value.classID,
        subjectID: data.value.subjectID,
        date: data.value.date,
        isLecture: this.isLecture,
        lecturerID: data.value.lecturerID,
        isOpen: this.isOpen,
        classPass: data.value.classPass,
        practiceGroup: data.value.practiceGroup
      }
      this.serviceStudent.updateClass(request)
        .subscribe(data => {
          if (data == true) {
            this.isLecture = false;
            this.isOpen = false;
            this.getClass();
            this.class.push(data);
          } else {
            alert("Morate popuniti sva polja!!!")
          }
        });
    }
    else if (this.activeModal == 'addStudent'){
      let request = {
        name: data.value.name,
        surname: data.value.surname,
        email: data.value.email,
        phone: data.value.phone,
        dob: data.value.dob,
        yearNo: data.value.yearNo,
        indexNo: data.value.indexNo,
        pass: data.value.pass,
        departmentID: data.value.departmentID
      }
      this.serviceStudent.addStudent(request)
        .subscribe(data => {
          if (data == true) {
            this.getStudent();
            this.class.push(data);
          } else {
            alert("Morate popuniti sva polja!!!")
          }
        });
    }
    else if (this.activeModal == 'addStudentAtte'){
      let request = {
        time: data.value.time,
        studentID: data.value.studentID,
        classID: data.value.classID
      }
      this.serviceStudent.addStudentAtte(request)
        .subscribe(data => {
          if (data == true) {
            this.getAttendances();
            this.class.push(data);
          } else {
            alert("Morate popuniti sva polja!!!")
          }
        });
    }
    else{
      let request = {
        studentID: data.value.studentID,
        name: data.value.name,
        surname: data.value.surname,
        email: data.value.email,
        phone: data.value.phone,
        dob: data.value.dob,
        yearNo: data.value.yearNo,
        indexNo: data.value.indexNo,
        pass: data.value.pass,
        departmentID: data.value.departmentID
      }
      this.serviceStudent.updateStudent(request)
        .subscribe(data => {
          if (data == true) {
            this.getStudent();
            this.class.push(data);
          } else {
            alert("Morate popuniti sva polja!!!")
          }
        });
    }
  }
  deleteStudents(studentID: number): void {
    // this.class = this.class.filter(h => h !== classID);
    let request = {
      studentID: studentID
    }
    this.serviceStudent.deleteStudent(request).subscribe(
      (res) => {
        console.log(res)
        this.getStudent();
      }
    );
  }

  deleteStudentsAte(attendancesID: number): void {
    // this.class = this.class.filter(h => h !== classID);
    let request = {
      attendancesID: attendancesID
    }
    this.serviceStudent.deleteStudentAtend(request).subscribe(
      (res) => {
        console.log(res)
        this.getAttendances();
      }
    );
  }


  delete(classID: number): void {
    // this.class = this.class.filter(h => h !== classID);
    let request = {
      classID: classID
    }
    this.serviceStudent.deleteHero(request).subscribe(
      (res) => {
        console.log(res)
        this.getClass();
      }
    );
  }

  /* save(data: any): void {
    let request = {
      isOpen: this.isOpen
    }
    this.serviceStudent.updateClass(request)
  } */



  radio(event: any) {
    if (event.toElement.id == 'isLecture') {
      this.isLecture = event.toElement.checked;
    } else {
      this.isOpen = event.toElement.checked;
    }
  }

  openCloseClass(type: any, id: number) {
    for (let i = 0; i < this.class.length; i++) {
      if (type == 'close' && id == this.class[i].classID) {
        this.add;
        this.class[i].isOpen = false;
      } else if (type == 'open' && id == this.class[i].classID) {
        this.class[i].isOpen = true;
      }
    }
  }

}
