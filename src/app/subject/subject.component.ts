import { Component, OnInit } from '@angular/core';
import { StudentService } from '../services/student.service';
import { Subject } from '../models/subject';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  subject: Subject[];
  constructor(private studentService: StudentService) { }

  ngOnInit() {
   return this.studentService.getSubjects()
    .subscribe(s => this.subject = s);
 
  }

}
